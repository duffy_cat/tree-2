using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //public Node targetNode;

    [Tooltip("Movement speed modifier.")]
    [SerializeField] private float speed = 3;
    private Node currentNode;
    private Vector3 currentDir;
    private bool playerCaught = false;

    public delegate void GameEndDelegate();
    public event GameEndDelegate GameOverEvent = delegate { };

    // Start is called before the first frame update
    void Start()
    {
        InitializeAgent();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerCaught == false)
        {
            if (currentNode != null)
            {
                //If beyond 0.25 units of the current node.
                if (Vector3.Distance(transform.position, currentNode.transform.position) > 0.25f)
                {
                    transform.Translate(currentDir * speed * Time.deltaTime);
                }
                else
                {
                    //currentNode = targetNode;
                    currentNode = DepthFirstSearch();
                    currentDir = currentNode.transform.position - transform.position;
                    currentDir = currentDir.normalized;
                }
            }
            else
            {
                Debug.LogWarning($"{name} - No current node");
            }

            Debug.DrawRay(transform.position, currentDir, Color.cyan);
        }
    }

    //Called when a collider enters this object's trigger collider.
    //Player or enemy must have rigidbody for this to function correctly.
    private void OnTriggerEnter(Collider other)
    {
        if (playerCaught == false)
        {
            if (other.tag == "Player")
            {
                playerCaught = true;
                GameOverEvent.Invoke(); //invoke the game over event
            }
        }
    }

    /// <summary>
    /// Sets the current node to the first in the Game Managers node list.
    /// Sets the current movement direction to the direction of the current node.
    /// </summary>
    void InitializeAgent()
    {
        currentNode = GameManager.Instance.Nodes[0];
        currentDir = currentNode.transform.position - transform.position;
        currentDir = currentDir.normalized;
    }

    //Implement DFS algorithm method here
    private Node DepthFirstSearch() 
    {
        Stack nodeStack = new Stack();
        List<Node> visitedNodes = new List<Node>();
        nodeStack.Push(GameManager.Instance.Nodes[0]);

        while(nodeStack.Count > 0) 
        {
            Node currentNode = (Node)nodeStack.Pop();
            visitedNodes.Add(currentNode);
            foreach (Node child in currentNode.Children) 
            {
                if(visitedNodes.Contains(child) == false && nodeStack.Contains(child) == false) 
                {
                    if (child == GameManager.Instance.Player.CurrentNode)
                    {
                        return child;
                    }
                    nodeStack.Push(child);
                }
            }
        }

        return null;
    }
}
