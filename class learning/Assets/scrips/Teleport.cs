using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public Transform spawnPort;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Ball") 
        {
            if(other.TryGetComponent(out Rigidbody rb) == true) 
            {
                rb.velocity = Vector3.zero;
            }
            other.transform.position = spawnPort.position;
        }
    }
}
