using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample : MonoBehaviour 
{
    private void Start()
    {
        Fish fish;
        fish = new Fish("Paratilapia polleni", 7);
        fish.PrintAnimal();
    }
}


public abstract class Animal
{
    private string name;
    private int age;

    public Animal(string _name, int _age) 
    {
        name = _name;
        age = _age;
    }

    public abstract void Eat();

    public abstract void Eat(string food);

    public virtual void PrintAnimal() 
    {
       Debug.Log("Name: " + name);
       Debug.Log("Age: " + age);
    }
}

public class Fish : Animal 
{
    public Fish(string _name, int _age) : base(_name, _age)
    {

    }

    public override void Eat(string food)
    {
        Debug.Log("Fish has eaten " + food);
    }

    public override void Eat() 
    {
        Debug.Log("fish is eating");
    }

    public override void PrintAnimal()
    {
        Debug.Log("this is fish");
    }
}