using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Heath : MonoBehaviour
{
    public float maxHealth;
    public Image healthBar;

    private float currentHeath = 0;

    public static Heath Instance { get; private set; }

    private void Awake()
    {
        if(Instance == null) 
        {
            Instance = this;
        }
        else 
        {
            enabled = false;
        }
    }

    void Start()
    {
        currentHeath = maxHealth;
        healthBar.fillAmount = currentHeath / maxHealth;
    }

    public void TakeDamage(float amount)
    {
        if(currentHeath > 0) 
        {
            currentHeath -= amount;
            Debug.Log(currentHeath);
            if(currentHeath <= 0) 
            {
                currentHeath -= amount;
                if(currentHeath <= 0) 
                {
                    currentHeath = 0;
                    int sceneIndex = SceneManager.GetActiveScene().buildIndex;
                    SceneManager.LoadScene(sceneIndex);
                }
            }
            healthBar.fillAmount = currentHeath / maxHealth;
        }
    }
}
