using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vil : MonoBehaviour
{
    void Start()
    {
        
    }
}

public abstract class Vehicle 
{
    int speed;

    public Vehicle(int _speed)
    {
        speed = _speed;
    }

    public virtual void Drive() 
    {
    
    }
}

public class Bmw : Vehicle
{
    public Bmw(int _speed) : base(_speed)
    {

    }

    public override void Drive()
    {
        Debug.Log("cant think of something for Bmw");
    }
}
public class Mazda : Vehicle
{
    public Mazda(int _speed) : base(_speed)
    {

    }

    public override void Drive()
    {
        Debug.Log("you are the Mazda of your destiny");
    }
}
public class Jeep : Vehicle
{
    public Jeep(int _speed) : base(_speed)
    {

    }

    public override void Drive()
    {
        Debug.Log("may the jeep be you");
    }
}
