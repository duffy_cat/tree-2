using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthFirstSearch : MonoBehaviour
{
    public Node rootNode;
    public Node target;

    private void Start()
    {
        int stepCount = DFS(target);
        if(stepCount > -1) 
        {
            Debug.Log(target.name + " was found in " + stepCount + " steps."); 
        }
        else 
        {
            Debug.Log(target.name + " was not found.");
        }
    }

    public int DFS(Node targetNote) 
    {
        Stack stack = new Stack();
        List<Node> visitedNodes = new List<Node>();

        stack.Push(rootNode);

        while (stack.Count > 0) 
        {
            Node node = (Node)stack.Pop();
            visitedNodes.Add(node);
            
            foreach (Node child in node.children) 
            {
                if(visitedNodes.Contains(child) == false && stack.Contains(child) == false) 
                {
                    if(child == targetNote) 
                    {
                        return visitedNodes.Count;
                    }
                    stack.Push(child);
                }
            }
        }
        return -1;
    }
}
