using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kick : MonoBehaviour
{
    public float kickDist;
    public float kickforce;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") == true) 
        {
            Debug.DrawRay(transform.position, transform.forward * kickDist, Color.red, 1.5f);
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, kickDist) == true) 
            {

                Vector3 dir = new Vector3(transform.forward.x, 0, transform.forward.z);
                Debug.DrawRay(transform.position, dir, Color.green, 1.5f);

                if(hit.collider.tag == "Ball") 
                {
                    if(hit.collider.TryGetComponent(out Rigidbody rb) == true) 
                    {
                        rb.AddForce(dir * kickforce, ForceMode.Impulse);
                    }
                }
            }
        }       
    }
}
