using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainManu : MonoBehaviour
{
    public int sceneIndex = 0;

    public void LoadNewGame() 
    {
        SceneManager.LoadScene(sceneIndex);
    }

    public void QuitGame() 
    {
        Application.Quit();
    }
}
