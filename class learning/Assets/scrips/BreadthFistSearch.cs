using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadthFistSearch : MonoBehaviour
{
    public Node rootNode;
    public Node target;

    private void Start()
    {
        int visitedNodes = BFS(target);
        if (visitedNodes > -1) 
        {
            Debug.Log(target.name + " visiting " + visitedNodes);
        }
        else 
        {
            Debug.Log(target.name + " not found");
        }
    }

    public int BFS(Node target)
    {
        Queue<Node> queue = new Queue<Node>();
        List<Node> visited = new List<Node>();
        queue.Enqueue(rootNode);
        visited.Add(rootNode);
           
        while (queue.Count > 0)
        {
            Node node = queue.Dequeue();
            foreach (Node child in node.children)
            {
                if (visited.Contains(child) == false)
                {
                    if (child == target)
                    {
                        return visited.Count;
                    }
                    visited.Add(child);
                    queue.Enqueue(child);
                }
            }
        }
        return -1;
    }
}
